# Wordpress UP

## Setup
#### File Modifications (as of now)
Edit the necessary lines in the `.env` file, that should be it...

#### Web Server Setup
Just make sure the host running these containers is setup to allow port 80, 443, and 8080. 
(8080 is only needed if you want to access the TRAEFIK dashboard, otherwise ignore.)

## Starting the engines
Run the `start.sh` on your host and it will start everything up.
