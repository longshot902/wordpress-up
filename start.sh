#!/bin/bash

# Create a blank ACME.json file (if not present) AND assign permissions
touch acme.json
chmod 600 acme.json

# Now start everything
docker-compose up -d
